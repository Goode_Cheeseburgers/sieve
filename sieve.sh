#!/bin/sh
# Sieve.sh - reads a textfile for dns blocklists, fetches them, removes duplicates and whitelisted urls
# then agrigates product into an output textfile.

BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BLOCKLISTS_FILE="blocklists.txt"
output_file="hosts_output.txt"

echo "----------------------------------------------------------------------------------------------"
echo "   Sieve - The host file cleaner"
echo "----------------------------------------------------------------------------------------------"
echo ""
# check dir perms
echo "Running peliminary checks..."

if [ -w $BASEDIR ]; then
    echo "Current directory permisions check...done."
else
    echo "Please check that you have write permissions on sieves directory"
    exit 1
fi

if [ -f $SAVEFILE ]; then
    echo "Output file located...done."
else
    echo "No such file as $SAVEFILE"
    exit 1
fi

if [ -w $SAVEFILE ]; then
        echo "Output file is writable...done."
    else
        echo "Please check that you have write permissions on output file"
        exit 1
fi

if [ -f "$BASEDIR/$BLOCKLISTS_FILE" ]; then
    echo "custom list found."
    URLS="$BASEDIR/$BLOCKLISTS_FILE"
else
    echo "custom list not found, using default list instead."
    URLS="$BASEDIR/default_lists.txt"
fi

# delete any previous hosts_tmp file
rm -R /temp 2> /dev/null && mkdir /temp
# set user agent for cloudflare
agent="Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36"

echo "Sieve is downloading all hosts from $URLS"

echo ""
echo "---------------------------------------------------------------------------------------------------"
for url in `cat $URLS`
    do
        echo $url...
        curl -s -L -A "${agent}" "${url}" >> temp/hosts_tmp.txt
        count1=$( wc -l <"temp/hosts_tmp.txt" )
        echo "$count1 hosts"
        echo "ok"
        echo "-------------------------------------------------------------------------------------------"
done

if [ -f "$BASEDIR/$BLOCKLISTS_FILE" ]; then
    <temp/hosts_tmp.txt awk -F '#' '{print $1}' | \
        awk -F '/' '{print $1}' | \
        awk '($1 !~ /^#/) { if (NF>1) {print $2} else {print $1}}' | \
        sed -nr -e 's/\.{2,}/./g' -e '/\./p' >  temp/hosts_tmp2.txt
fi

awk '!seen[$0]++' temp/hosts_tmp2.txt > $output_file

# Clean up the temp files
rm -R temp

count=$( wc -l <"$output_file" )
echo ""
echo "-----------------------------------------------------"
echo "      $count hosts pulled into $output_file"
echo "-----------------------------------------------------"
